<?php

Breadcrumbs::for('admin.vendor.index', function ($trail) {
    $trail->push(__('strings.backend.dashboard.title'), route('admin.dashboard'));
});
