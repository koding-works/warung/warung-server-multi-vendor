<?php

use App\Http\Controllers\Backend\VendorController;

// All route names are prefixed with 'admin.auth'.
Route::group([
    'prefix' => 'vendor',
    'as' => 'vendor.',
    'namespace' => 'Vendor',
    'middleware' => 'role:'.config('access.users.admin_role'),
], function () {
    Route::get('/', [VendorController::class, 'index'])->name('index');
    Route::get('/create', [VendorController::class, 'create'])->name('create');
    Route::post('/', [VendorController::class, 'store'])->name('store');
    Route::get('/{id}/edit', [VendorController::class, 'edit'])->name('edit');
    Route::patch('/{id}', [VendorController::class, 'update'])->name('update');
    Route::delete('/{id}', [VendorController::class, 'destroy'])->name('destroy');
});
