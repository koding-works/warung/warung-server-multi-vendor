<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::group(['namespace' => 'v1', 'prefix' => 'v1', 'as' => 'v1.'], function () {
    Route::group(['namespace' => 'customer', 'prefix' => 'customer', 'as' => 'customer.'], function () {
        include_route_files(__DIR__.'/api/v1/customer');
    });
});

// Route::group(['namespace' => 'v2', 'prefix' => 'v2', 'as' => 'v2.'], function () {
//    Route::group(['namespace' => 'customer', 'prefix' => 'customer', 'as' => 'customer.'], function () {
//     include_route_files(__DIR__.'/api/customer');
// });
// });
